#! /usr/bin/env python3
import os
import subprocess
from time import sleep
import dbus


class BatteryMonitor:
    SUPPLY_PATH = "/sys/class/power_supply/"
    UEVENT_FILE = "uevent"

    def __init__(self):
        self.state = self.get_charging_state()
        self.level = self.get_charging_level()

        self.state_notifications = NotificationInterface()
        self.level_notifications = NotificationInterface()

        self.state_notifications.send_notification('Battery monitor started',
                                                   'Battery monitor is now running and watching your battery.',
                                                   NotificationInterface.URGENCY_LOW,
                                                   NotificationInterface.EXPIRES_DEFAULT)

        self.callback_critical = None
        self.callback_suspend = None

    @staticmethod
    def get_supplies(prefix=""):
        return [dir for dir in os.listdir(BatteryMonitor.SUPPLY_PATH) if dir.startswith(prefix)]

    @staticmethod
    def get_state(supply):
        state = {}
        with open(os.path.join(BatteryMonitor.SUPPLY_PATH, supply, BatteryMonitor.UEVENT_FILE), 'r') as f:
            for line in f.readlines():
                k, v = line.replace('POWER_SUPPLY_', '').split('=', 1)
                v = v.strip()
                state[k.lower()] = v if not v.isdigit() else int(v)

        return state

    def set_critical_callback(self, callback):
        self.callback_critical = callback

    def set_suspend_callback(self, callback):
        self.callback_suspend = callback

    def get_charging_state(self):
        state = 'unknown'

        '''
        We somehow need to combine n states into one. The states can be one of unknown, charging, discharging and full.
        Obviously, if one is discharging, this should take preference. Charging should overwrite full.
        '''

        batteries = self.get_supplies('BAT')
        states = [self.get_state(battery) for battery in batteries]
        for s in states:
            if state != "charging" and s['status'] == 'Full':
                state = 'full'

            if s['status'] == 'Charging':
                state = 'charging'

            if s['status'] == 'Discharging':
                state = 'discharging'
                break

        return state

    def get_charging_level(self):
        level = 0
        batteries = self.get_supplies('BAT')

        if not batteries:
            return None

        for battery in batteries:
            level += self.get_state(battery)['capacity']

        return round(level / len(batteries))

    def loop(self):
        state = self.get_charging_state()
        level = self.get_charging_level()

        if self.state != state:
            print(f"Battery is now {state}", flush=True)
            self.state_notifications.send_notification("Battery state changed", f"Battery is now {state}.",
                                                       NotificationInterface.URGENCY_LOW,
                                                       NotificationInterface.EXPIRES_DEFAULT)

            # check if state is charging and reset previous level messages
            if state == 'charging':
                self.level_notifications.close_notification()

        if state == 'discharging' and self.level != level:
            if level < 5:
                print('Critical Battery, suspending!', flush=True)
                self.level_notifications.send_notification("Critical battery!", "Suspending!",
                                                           NotificationInterface.URGENCY_CRITICAL,
                                                           NotificationInterface.EXPIRES_NEVER)

                if self.callback_suspend:
                    self.callback_suspend()

                subprocess.call(['/usr/bin/systemd-run', '--user', '/bin/systemctl', 'suspend'])
            elif level < 8:
                print('Low Battery, your computer will suspend soon!', flush=True)
                self.level_notifications.send_notification("Low battery!", "Computer will suspend soon!",
                                                           NotificationInterface.URGENCY_CRITICAL,
                                                           NotificationInterface.EXPIRES_NEVER)

                if self.callback_critical:
                    self.callback_critical(level)
            elif level < 20:
                print(f'Low Battery, {level}% remaining.', flush=True)
                self.level_notifications.send_notification("Low battery.", f'Low Battery, <b>{level}</b>% remaining.',
                                                           NotificationInterface.URGENCY_LOW,
                                                           NotificationInterface.EXPIRES_NEVER)
        self.state = state
        self.level = level

    def loopforever(self):
        while True:
            self.loop()
            sleep(2)


class NotificationInterface:
    EXPIRES_DEFAULT = -1
    EXPIRES_NEVER = 0

    URGENCY_LOW = 0
    URGENCY_NORMAL = 1
    URGENCY_CRITICAL = 2

    def __init__(self, appname=os.path.basename(__file__)):
        self.appname = appname
        self.id = 0
        self.iface = dbus.Interface(
            object=dbus.SessionBus().get_object("org.freedesktop.Notifications", "/org/freedesktop/Notifications"),
            dbus_interface="org.freedesktop.Notifications")

    def send_notification(self, title, body, urgency, expires):
        self.id = self.iface.Notify(self.appname,
                                    self.id,
                                    "",  # icon
                                    title,
                                    body,
                                    [],  # events
                                    {'urgency': dbus.Byte(urgency)},  # hints
                                    expires)

    def close_notification(self):
        self.iface.CloseNotification(self.id)


if __name__ == '__main__':
    batmon = BatteryMonitor()
    batmon.loopforever()
