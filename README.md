# psybatmon

Simple battery monitoring script capable of dealing with multiple batteries. Uses dbus for notifications, can handle callback on critical state and system suspend.

To use it as it is, clone it to `/opt/` (or a custom folder, but don't forget to change the path in the .service file). Copy the service file to `/etc/systemd/system/` and change the user withing the file to your user. Create a virtual environment inside your cloned folder and install dependencies:
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## customization
create your own .py file, import the `BatteryMonitor` and have fun:

```python
#! /usr/bin/env python3
import subprocess
from batmon import BatteryMonitor


def on_critical(level):
    subprocess.call(['/usr/bin/mpv', '/home/foo/beedoo.mp3'],
                    env={'XDG_RUNTIME_DIR': '/run/user/1000'})


'''
This is normaly done by your locker listening to the dbus-suspend signal. If it doesn't 
you can use the following as a workaround.
'''
def on_suspend():
    subprocess.call(['/usr/bin/i3lock', '-c', '000000', '-i', '/home/foo/lockscreen.png', '-t'])


if __name__ == '__main__':
    batmon = BatteryMonitor()
    batmon.set_critical_callback(on_critical)
    batmon.set_suspend_callback(on_suspend)
    batmon.loopforever()
```